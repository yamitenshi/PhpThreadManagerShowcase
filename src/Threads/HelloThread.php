<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 20:45
 */

namespace ThreadShowcase\Threads;

use ThreadShowcase\Events\GreetingChangeEvent;
use ThreadShowcase\Events\IgnoredEvent;
use YamiTenshi\ThreadManager\Event\BaseEvent;
use YamiTenshi\ThreadManager\Thread\Thread;
use YamiTenshi\ThreadManager\ThreadManager;

/**
 * Class HelloThread
 *
 * A basic Hello World application in thread form, with event handling
 *
 * @package ThreadShowcase\Threads
 */
class HelloThread extends Thread
{
    private $greeting = 'Hello thread!';
    private $numRuns;

    /**
     * HelloThread constructor.
     *
     * Takes the number of times it should greet the user as an argument
     *
     * @param int $numRuns
     */
    public function __construct($numRuns = 10)
    {
        $this->numRuns = $numRuns;
        $this->setLogicHandler($this->greet());
    }

    public function handleEvent(BaseEvent $event)
    {
        if ($event->getName() === 'greetingChange') {
            /** @var GreetingChangeEvent $event */
            $this->setGreeting($event->getNewGreeting());
        }
    }

    /**
     * @param string $greeting
     */
    public function setGreeting($greeting)
    {
        $this->greeting = $greeting;
    }

    /**
     * The internal logic handler for the HelloThread
     *
     * Greets the user with the greeting set to this thread, and sets a new greeting through an event handler
     *
     * @return \Generator
     */
    private function greet()
    {
        for ($i = 0; $i < $this->numRuns; $i++)
        {
            print $this->greeting . ' from PID ' . $this->getPID() . PHP_EOL;
            ThreadManager::getInstance()->triggerEvent(new GreetingChangeEvent('New greeting set by PID ' . $this->getPID()));
            ThreadManager::getInstance()->triggerEvent(new IgnoredEvent());
            yield null;
        }
        yield 0;
    }

    public function init()
    {
        print 'Initializing ' . __CLASS__ . PHP_EOL;
    }

    public function terminate()
    {
        print 'Cleaning up ' . __CLASS__ . PHP_EOL;
    }
}