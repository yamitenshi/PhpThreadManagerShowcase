<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 20:49
 */

namespace ThreadShowcase\Events;

use YamiTenshi\ThreadManager\Event\BaseEvent;

class GreetingChangeEvent extends BaseEvent
{
    protected $name = 'greetingChange';

    /** @var string */
    protected $newGreeting;

    public function __construct($newGreeting)
    {
        $this->newGreeting = $newGreeting;
    }

    /**
     * @return string
     */
    public function getNewGreeting()
    {
        return $this->newGreeting;
    }
}