<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 15-3-16
 * Time: 21:01
 */

namespace ThreadShowcase\Events;

use YamiTenshi\ThreadManager\Event\BaseEvent;

class IgnoredEvent extends BaseEvent
{
    protected $name = 'ignore';
}