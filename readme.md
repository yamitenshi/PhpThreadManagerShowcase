# PHP Thread Manager showcase

This application showcases the PHP Thread Manager library at https://gitlab.com/yamitenshi/PhpThreadManager.

It initializes a thread manager with three threads, two of which are the thread equivalent of a Hello World application.
The first will terminate without errors after two executions, the second will attempt to run ten times.

The third thread is a dynamically BasicThread that runs a generator defined in index.php.
This thread will trigger an error after four executions, causing the thread manager to shut the whole thing down.

Clone this repository, run `composer install` to install the PHP Thread Manager library, and run the example with `php index.php`