<?php
/**
 * Created by PhpStorm.
 * User: yamitenshi
 * Date: 14-3-16
 * Time: 23:36
 */

require_once "vendor/autoload.php";

function threadLogic() {
    print 'This comes first' .  PHP_EOL;
    yield null;
    print 'This comes second' . PHP_EOL;
    yield null;
    print 'This comes third' . PHP_EOL;
    yield null;
    print 'And this will be an error' . PHP_EOL;
    yield 1;
}

$threadManager = \YamiTenshi\ThreadManager\ThreadManager::getInstance();

$threadManager->registerThread(new \ThreadShowcase\Threads\HelloThread(2));
$threadManager->registerThread(new \ThreadShowcase\Threads\HelloThread());
$threadManager->registerThread(new \YamiTenshi\ThreadManager\Thread\BasicThread(threadLogic()));

try {
    $threadManager->execute();
} catch(\YamiTenshi\ThreadManager\Exceptions\ExecutionException $ex) {
    die($ex->getMessage() . ': ' . $ex->getPrevious()->getMessage() . PHP_EOL);
}

print 'All threads have finished without problems, and all is well in the world...' .  PHP_EOL;